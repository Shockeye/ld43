﻿using UnityEngine;
using UnityEngine.UI;

namespace Shockeye.Menu
{
    /// <summary>
    /// Menu Component class. 
    /// An object of this type must be the child of an object with the MenuSystem component
    /// </summary>
    public class Menu : MonoBehaviour
    {
        MenuSystem menuSystem;

        public Text title;
        public Text message;
        public Button submit;
        public Button cancel;
 

        private void Start()
        {
            menuSystem = GetComponentInParent<MenuSystem>();

            if (menuSystem == null)
            {
                Debug.LogError("Missing MenuSystem");
            }

            if(cancel != null)
            {
                //this is our default cancel action
                cancel.onClick.RemoveAllListeners();
                cancel.onClick.AddListener(Cancel);
            }
        }

        private void Update()
        {
            if (Input.GetButtonDown("Cancel"))
            {
                Cancel();
            }
        }

        public void Cancel()
        {
            Debug.Log("Menu Cancelled");
            menuSystem.Previous();
        }

        public bool HasTitle
        {
            get { return (title != null); }
        }

        public bool HasMessage
        {
            get { return (message != null); }
        }

        public string Title
        {
            set { if (HasTitle) title.text = value; }
            get
            {
                if (HasTitle) return title.text;
                else return "";
            }
        }

        public string Message
        {
            set { if (HasMessage) message.text = value; }
            get
            {
                if (HasMessage) return message.text;
                else return "";
            }
        }

        public bool HasSubmitButton
        {
            get { return (submit != null); }
        }

        public bool HasCancelButton
        {
            get { return (cancel != null); }
        }

    }
}
