﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : Entity
{

    void Start()
    {
        Description = "Waypoint";

    }

    override public int Type { get { return 2; } }

    override public bool Act(Game game)
    {
        return true;
    }

    override public void Reset()
    {

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 1);
    }

}
