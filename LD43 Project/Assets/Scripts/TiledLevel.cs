﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
/// <summary>
/// Top down view Y is up XZ plane is level
/// </summary>
/// 
[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class TiledLevel : MonoBehaviour
{
    
    [System.Serializable]
    public class Tile //: ISerializationCallbackReceiver

    {
        public Vector2Int coordinates = Vector2Int.zero;
        public List<int> contents = new List<int>();//instanceID of entity
        //public List<Entity> contents = new List<Entity>();
        public Tile(Vector2Int coordinates)
        {
            this.coordinates = coordinates;
        }

        public void AddEntity(Entity e, int index)
        {
            //Todo: check for duplicates, ie when rebuilding from saved level data
            //Could we change contents from List<> to HashSet<> to do this automagicly?
            //problem is we lose order which could be useful for stacking objects in grid square

            if (!contents.Contains(index))
            {
                contents.Add(index);
            }

            e.transform.position = TiledLevel.GridToWorld(coordinates);
            e.transform.position += e.offset;
            e.coordinates = coordinates;
           
        }


        public override string ToString()
        {
            return coordinates.ToString();
        }

        
    }

    



    #region 
   
    public List<Tile> tileList = new List<Tile>();

    [SerializeField]
    int entitySpawns = 0;

    [HideInInspector]
    public Vector2Int min = Vector2Int.zero;

    [HideInInspector]
    public Vector2Int max = Vector2Int.zero;

    public List<Transform> prefabs = new List<Transform>();//prefab protypes for entities
    List<Entity> entities = new List<Entity>();

    Tile selected;

    MeshRenderer meshRenderer;
    MeshFilter meshFilter;
    public Material material;

    public string fileName;

    public Tile Selected
    {
        get
        {
            return selected;
        }

        private set
        {
            selected = value;
        }
    }
    #endregion
    private void OnDrawGizmosSelected()
    {

        if (Selected != null)
        {
            Gizmos.color = Color.yellow;
            Vector3 selectedPos = GridToWorld(Selected.coordinates);
            selectedPos.y += 1.0f;//offset to put base of selection box at floor level
            Gizmos.DrawWireCube(selectedPos , new Vector3(1.0f, 2.0f, 1.0f));
            

            
        }

    }
    
    public int AddEntity(Entity e, int prefabID, Vector2Int gridPosition)
    {
        if (TileExistsAt(gridPosition))
        {
            Tile tile = GetTileAt(gridPosition);
            tile.AddEntity(e, prefabID);
            e.prefabID = prefabID;
            e.instanceID = entitySpawns++;
            entities.Add(e);
            return e.instanceID;

        }
        else return -1;
    }
    
    public Entity GetEntity(int id)
    {
        
        return entities.Find(x => x.instanceID == id);
        
    }

    public void MoveEntity(int entityId, Vector2Int destination)
    {
        Entity e = GetEntity(entityId);

        if(e != null)
        {
            MoveEntity(e, destination);
        }
    }

 
    public void MoveEntity(Entity entity, Vector2Int destination)
    {
        if (TileExistsAt(destination))
        {
            Tile src = GetTileAt(entity.coordinates);
            src.contents.Remove(entity.instanceID);
            Tile dest = GetTileAt(destination);
            dest.contents.Add(entity.instanceID);
            entity.coordinates = destination;
            entity.transform.position = TiledLevel.GetGridCentrePoint(destination);
        }
    }
 
 
    public int AddPrefab(Transform transform)
    {
        //check if this already exists, if not add to list

        //research implementation of this, as I don't think this will work as intended
        //ie. matching stored transform may prove somewhat dodgy
        //it turns out it does work, so far. we'll see once file serialization of entities is added...

        int index = -1;
        if(prefabs.Contains(transform))
        {
            Debug.Log("existing prefab");
            index = prefabs.FindIndex(x => x == transform);
        }
        else
        {
            Debug.Log("adding new prefab");
            prefabs.Add(transform);
            index = prefabs.Count - 1;
        }

        

        return index;
    }

    private void OnDrawGizmos()
    {
        //this whole it is kind of redundant now we build mesh in editor
        //perhaps remove? unless we see potential utility in outline around grid squares.

        if (Selection.activeObject != this.gameObject)
        {
            foreach (var item in tileList)
            {
                //todo: just draw a square 
                if (Selected != null && item.coordinates == Selected.coordinates)
                {
                    Gizmos.color = Color.green;
                }
                else
                {
                    Gizmos.color = Color.yellow;
                }
                
                Gizmos.DrawWireCube(GridToWorld(item.coordinates), new Vector3(1.0f, 0.1f, 1.0f));

            }
        }

    }

    public static Vector3 GridToWorld(int x, int y)
    {
        //old version from GetPointInGrid 
        //Vector3 point = new Vector3(gridPosition.x + 0.5f, 0.0f, gridPosition.y + 0.5f);
        //return point;

        //TODO: generalise the offsets

        return new Vector3(x + 0.5f, 0, y + 0.5f);
    }
    public static Vector3 GridToWorld(Vector2Int coordinates)
    {
       return GridToWorld(coordinates.x, coordinates.y);
    }

    public static Vector2Int WorldToGrid(Vector3 position)
    {
        //return the grid coordinates 
        //this is a point in the XZ plane. Y is up so converting to a 3d display will be easier.
        Vector2Int gridPosition = new Vector2Int((int)position.x, (int)position.z);
        return gridPosition;
    }

    [ObsoleteAttribute("will be replaced by WorldToGrid", false)]
    static public Vector2Int GetPointInGrid(Vector3 point)
    {
        //return the grid coordinates 
        //this is a point in the XZ plane. Y is up so converting to a 3d display will be easier.
        Vector2Int gridPosition = new Vector2Int((int)point.x, (int)point.z);
        return gridPosition;
    }

    static public Vector3 GetGridCentrePoint(Vector2Int gridPosition)
    {
        Vector3 point = new Vector3(gridPosition.x + 0.5f, 0.0f, gridPosition.y + 0.5f);

        return point;
    }


    // Use this for initialization
    void Start ()
    {
        UnitTests();
        Debug.Log("TiledLevel.Start");
        if (!String.IsNullOrEmpty(fileName))
        {
            ClearLevel();
            Transform[] children = GetAllChildren();            
            for (int i = children.Length - 1; i >= 0; i--)
            {
                Destroy(children[i].gameObject);
            }

            Load(fileName);
        }
        InitMesh();
        BuildMesh();
    }

    private void UnitTests()
    {
        if(TileExistsAt(Vector2Int.zero))
        {
            Debug.Log("origin zero tile test ok");
        }
        else { Debug.Log("origin zero tile test fail"); }
    }

    public void ClearLevel()
    {
        
        tileList.Clear();
        entities.Clear();
        entitySpawns = 0;
        //meshFilter.mesh = null;
        min = Vector2Int.zero;
        max = Vector2Int.zero;
        transform.position = Vector3.zero; //TODOD: this whole bit duplicates reset . needs rethink
        Initialize();
        BuildMesh();



    }

    public Transform[] GetAllChildren()
    {
        Transform[] children = new Transform[transform.childCount];
        int childindex = 0;
        foreach (Transform child in transform)
        {
            children[childindex] = child;
            childindex++;
        }

        return children;
    }

    // Update is called once per frame
    void Update ()
    {

    }

    /// <summary>
    /// puts the level at origin
    /// Adds the first empty tile to the level on creation
    /// </summary>
    void Reset()//TODO: we dont always want to clear everything - need to check status
    {
        tileList.Clear();
        min = Vector2Int.zero;
        max = Vector2Int.zero;
        transform.position = Vector3.zero;
        Initialize();
    }

    public void Initialize()
    {
        AddTile(Vector2Int.zero, new Tile(Vector2Int.zero));     
    }

    public void SelectTile(Vector2Int item)
    {
        if(tileList.Exists(x => x.coordinates == item))
        {
            selected = tileList.Find(x => x.coordinates == item);///map[item];
            EditorUtility.SetDirty(this);//maybe not use this method. see Scripting API.
        }

        
         
    }

    public void AddTile(Vector2Int coordinates)
    {
        AddTile(coordinates, new Tile(coordinates));
    }

    public void AddTile(Vector2Int coordinates, Tile tile)
    {
        if (!tileList.Exists(x => x.coordinates == coordinates))
        {
            tileList.Add(tile);
            //map.Add(coordinates, tile);
            if (coordinates.x < min.x) min.x = coordinates.x;
            if (coordinates.y < min.y) min.y = coordinates.y;
            if (coordinates.x > max.x) max.x = coordinates.x;
            if (coordinates.y > max.y) max.y = coordinates.y;
        }
        SelectTile(coordinates);
    }

    public Vector2Int[] GetEmptyGridPositions()
    {
        List<Vector2Int> result = new List<Vector2Int>();
        for (int x = min.x-1; x < max.x +2; x++)
        {

            for (int y = min.y-1; y < max.y+2; y++)
            {
                Vector2Int coordinates = new Vector2Int(x, y);
                if (!tileList.Exists(t => t.coordinates == coordinates))
                {
                    result.Add(coordinates);
                }
            }
        }
        return result.ToArray();
    }

    public Vector2Int[] GetTileGridPositions()
    {
        List<Vector2Int> result = new List<Vector2Int>();
        for (int x = min.x; x < max.x + 1; x++)
        {

            for (int y = min.y; y < max.y + 1; y++)
            {
                Vector2Int coordinates = new Vector2Int(x, y);
                if (tileList.Exists(t => t.coordinates == coordinates))
                {
                    result.Add(coordinates);
                }
            }
        }
        return result.ToArray();
    }

    //hmm this method which lets the class expose map funtionality is now misleadingly named.
    //these are called by Game class. I originally had a simple dictionary map in Game rather than
    //a Level class. when lvel was first added I wrapped dictionary so Game code didn't need changing.
    //TODO: change Game code

    [ObsoleteAttribute("Use TileExistsAt(Vector2Int)", false)]
    public bool ContainsKey(Vector2Int gridPosition)
    {
        return (tileList.Exists(t => t.coordinates == gridPosition));

        //return map.ContainsKey(gridPosition);
    }

    public bool TileExistsAt(Vector2Int gridPosition)
    {
        return (tileList.Exists(t => t.coordinates == gridPosition));

    }

    [ObsoleteAttribute("Level Index accessor will be removed soon", false)]
    public Tile this[Vector2Int i]
    {
        /*
        get { return map[i]; }
        set { map[i] = value; }
        */

        get { return tileList.Find(t => t.coordinates == i); }
        set { AddTile(i, value); }
    }

    public Tile GetTileAt (Vector2Int gridPosition)
    {
        return tileList.Find(t => t.coordinates == gridPosition); 
        
    }

    public void InitMesh()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.material = material;
        meshFilter = GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        meshFilter.mesh = mesh;

    }
    public void BuildMesh() //really only the TiledLevelEditor class requires access
    {
        InitMesh();
        //assumes each grid square is 1x1 units.
        List<Vector3> newVertices = new List<Vector3>();
        List<Vector2> newUV = new List<Vector2>();
        List<int> newTriangles = new List<int>();

        HashSet<Vector3> verticesSet = new HashSet<Vector3>();
        //Debug.Log("BuildMesh/n");
        foreach (var item in tileList)
        {
            //Debug.Log(item);
            int index, bl, tl, tr, br;
            Vector3 centre = GridToWorld(item.coordinates);
            Vector3 vertex = Vector3.zero;
            //bottom left    
            vertex.x = centre.x - 0.5f;
            vertex.z = centre.z - 0.5f;

            index = newVertices.FindIndex(x => x == vertex);
            if(index < 0)
            {
                newVertices.Add(vertex);
                newUV.Add(new Vector2(item.coordinates.x, item.coordinates.y));
                index = newVertices.Count - 1;
            }

            
            bl = index;
            //top left    
            vertex.x = centre.x - 0.5f;
            vertex.z = centre.z + 0.5f;
            index = newVertices.FindIndex(x => x == vertex);
            if (index < 0)
            {
                newVertices.Add(vertex);
                newUV.Add(new Vector2(item.coordinates.x, item.coordinates.y + 1f));
                index = newVertices.Count - 1;
            }
            
            tl = index;
            
            //top right
            vertex.x = centre.x + 0.5f;
            vertex.z = centre.z + 0.5f;
            index = newVertices.FindIndex(x => x == vertex);
            if (index < 0)
            {
                newVertices.Add(vertex);
                newUV.Add(new Vector2(item.coordinates.x + 1f, item.coordinates.y + 1f));
                index = newVertices.Count - 1;
            }
            
            tr = index;
            //bottom right
            vertex.x = centre.x + 0.5f;
            vertex.z = centre.z - 0.5f;
            index = newVertices.FindIndex(x => x == vertex);
            if (index < 0)
            {
                newVertices.Add(vertex);
                newUV.Add(new Vector2(item.coordinates.x + 1f, item.coordinates.y));
                index = newVertices.Count - 1;
            }
            
            br = index;

            
            //build quad
            int[] quad = {tl,tr,bl,bl,tr,br };
            newTriangles.AddRange(quad);
        }


        meshFilter.sharedMesh.vertices = newVertices.ToArray();
        meshFilter.sharedMesh.uv = newUV.ToArray();
        meshFilter.sharedMesh.triangles = newTriangles.ToArray();
        
        meshFilter.sharedMesh.RecalculateNormals();


    }

    [System.Serializable]
    private class Wrapper
    {
        public Tile[] items;
        public int[] entities;
    }

    //TODO: refactoring: take a TiledLevel instance, we need acces to entities as well as tile list
    public void Export(string filename)// List<Tile> tileList)
    {
        
        string path = Application.persistentDataPath + "/Saves";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        path += "/";
        path += filename;


        //List<string> data = new List<string>();
        Wrapper data = new Wrapper();

        int[] entityLookup = new int[entities.Count];
        //Debug.Log(entitySpawns);
        for (int i = 0; i < entities.Count; i++)
        {
            string msg = "Entity:";
            msg += i.ToString();
            Debug.Log(msg);
            Entity ent = entities[i];
            msg = "Entity ID:";
            msg += ent.instanceID.ToString();
            Debug.Log(msg);
            entityLookup[ent.instanceID] = ent.prefabID;
        }

        
        data.items = tileList.ToArray();
        data.entities = entityLookup;
        

        using (FileStream file = File.Create(path))
        {

            string json = JsonUtility.ToJson(data, true);
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(file))
            {
                writer.Write(json);
                writer.Flush();
            }

        }
    }


    public void Load(string level_filename)
    {
        Debug.Log("TiledLevel.Load");
        string path = Application.persistentDataPath + "/Saves";
        path += "/";
        path += level_filename;
        string json;

        using (System.IO.StreamReader reader = new System.IO.StreamReader(path))
        {
            json = reader.ReadToEnd();
            
        }

        Wrapper data = JsonUtility.FromJson<Wrapper>(json);
        //RebuildLevel(data);
        
        
        foreach(Tile item in data.items)
        {
            AddTile(item.coordinates,item);
        }


        foreach (Tile tile in tileList)
        {
            foreach (int instanceID in tile.contents)
            {
                //spawn the appropriate entity
                int prefabID = data.entities[instanceID];
                SpawnEntity(prefabID, instanceID, tile.coordinates);
            
            }
        }
        
        //build mesh here????
        
    }

    
    public void SpawnEntity(int prefabID, int instanceID, Vector2Int gridPosition)
    {
        if (TileExistsAt(gridPosition))
        {
          
            Transform objTransform = (Transform)PrefabUtility.InstantiatePrefab(prefabs[prefabID]);//,, level.gameObject.transform);
            if (objTransform != null)
            {
                objTransform.parent = gameObject.transform;
                Entity e = objTransform.gameObject.GetComponent<Entity>();
                if (e != null)
                {
                    Tile tile = GetTileAt(gridPosition);
                    tile.AddEntity(e, prefabID);
                    e.prefabID = prefabID;
                    e.instanceID = instanceID;
                    entities.Add(e);           
                    if(!(entitySpawns > instanceID))
                    {
                        entitySpawns = instanceID + 1;
                    }
                }

            }
            

        }
        
    }
    
    

}
