﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[System.ObsoleteAttribute("Use TiledLevelEditor system", true)]
public class EditorTile : MonoBehaviour
{
    
    public Vector2Int gridPosition;
	public Tile tile = new Tile();
}
