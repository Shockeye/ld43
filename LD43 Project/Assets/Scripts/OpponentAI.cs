﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// controls enemy
/// possibly AI can be configured using ScriptableObjects in editor
/// in which case it should be a MonoBehavior.
/// But if not, I don't think it needs to be.
/// </summary>
public class OpponentAI : MonoBehaviour //does this need to be a monobehavior? I suspect not.
{
    List<Entity> entities = new List<Entity>(); //list of entities cotrolled by this AI

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    { 
		
	}

    /// <summary>
    /// this is called when it is the AIs turn to go.
    /// returns true when AI has finished turn.
    /// </summary>
    public bool Go()
    {
        return false;// true; //return true when finished turn
    }
}
