﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Shockeye
{
    public class Core : MonoBehaviour
    {
        
        
        static public void Quit()
        {
            

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;

#elif (UNITY_WEBGL)

#else
        Application.Quit();
#endif
        }

        static public void Screenshot()
        {
            string dir = Application.persistentDataPath + "/Screenshots";
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            string timestamp = string.Format("{0:yyyyMMddHHmmss}", System.DateTime.Now);

            string filename = dir + "/img" + timestamp + ".png";
            
            ScreenCapture.CaptureScreenshot(filename);
            Debug.Log(filename);
            //TODO: play camera shutter noise
        }
        
        static public void EditorPause()
        {

#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPaused = true;

#endif
        }
    }
}