﻿using System.Collections.Generic;
using UnityEngine;

namespace Shockeye.Menu
{
    public class MenuSystem : MonoBehaviour
    {
        Stack<Menu> menus;

        /// <summary>
        /// These special menus can be called from code, are reusable
        /// TODO?: implement as prefabs and instantiate at run-time?
        /// Should these be here or in client Game?
        /// </summary>
        public Menu confirmationDialog; //special menu, confirms action
        public Menu selectionDialog; //menu with selection list



        public Menu root;
        Menu current;


        void Awake()
        {
            menus = new Stack<Menu>();
            Root();
        }

        public void Root()
        {
            Hide();
            menus.Clear();
            SetCurrent(root);
            Show();
        }

        public void Clear()
        {
            Hide();
            current = null;
            menus.Clear();
        }

        public void Show()
        {
            if (current != null)
            {
                current.gameObject.SetActive(true);
            }
        }

        public void SetCurrent(Menu menu)
        {
            Hide();

            if (current != null)
            {
                menus.Push(current);
            }

            current = menu;

            Show();

        }

        public void Previous()
        {
            Debug.Log(menus.Count);
            if (menus.Count > 0)
            {
                Hide(); //close current menu
                current = menus.Pop(); // pop previous menu off the stack
                Show();
            }
        }

        public void Hide()
        {
            if (current != null)
            {
                current.gameObject.SetActive(false); //close current menu
                //current = null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="title">Title of dialog, if supported. If not required use empty string. </param>
        /// <param name="prompt">Message prompt of dialog, if supported. If not required use empty string.</param>
        /// <param name="action">Delegate to be called when confirmation recieved</param>
        public void ConfirmationDialog(string title, string prompt, UnityEngine.Events.UnityAction action)
        {
            
            if (title != null && confirmationDialog.HasTitle)
            {
                confirmationDialog.Title = title;
            }

            if (prompt != null && confirmationDialog.HasMessage)
            {
                confirmationDialog.Message = prompt;
            }

            if (action != null && confirmationDialog.HasSubmitButton)
            {
                confirmationDialog.submit.onClick.RemoveAllListeners();
                confirmationDialog.submit.onClick.AddListener(action);
            }

            if (confirmationDialog.HasCancelButton)
            {                
                confirmationDialog.cancel.onClick.RemoveAllListeners();
                confirmationDialog.cancel.onClick.AddListener(Previous);
            }

            SetCurrent(confirmationDialog);

        }

    }
}
