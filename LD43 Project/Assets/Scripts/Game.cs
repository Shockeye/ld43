﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Game : MonoBehaviour
{
    public Canvas displayOverlay;
    public Canvas playControlOverlay;
    public Transform toolTipText;
    public Transform highlight;
    OpponentAI opponent;

    Player player; 

    
    [HideInInspector]
    //public Level map;
    public TiledLevel map;
    

    // list of entities in the current level
    List<Entity> entities = new List<Entity>();


    
    public bool isPlayerTurn
    {
        get {return player.isPlayerTurn; }
        
    }
   
    
    Queue<IGameAction> actionQueue = new Queue<IGameAction>();// not really int this is just place holder
    
    // Use this for initialization
    void Start()
    {
        //should implement a more robust set up process with checks & balances
        map = GetComponent<TiledLevel>();
        player = GetComponent<Player>();
        opponent = GetComponent<OpponentAI>();

        Cursor.visible = true;
        if (displayOverlay != null && toolTipText != null)
        {
            toolTipText = Instantiate(toolTipText, displayOverlay.transform);
            toolTipText.gameObject.SetActive(false);
        }

        if (displayOverlay != null && highlight != null)
        {
            highlight = Instantiate(highlight);
            highlight.gameObject.SetActive(false);

        }
        
    }

    // Update is called once per frame
    void Update()
    {
        //debugging
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Shockeye.Core.EditorPause();
        }

#endif
        if (Input.GetKeyDown(KeyCode.F12))
        {
            Shockeye.Core.Screenshot();
        }

        if (entities.Count > 0)
        {
            Camera.main.transform.position = new Vector3(entities[0].coordinates.x, Camera.main.transform.position.y, entities[0].coordinates.y);
        }

        Vector2Int gridPos = GetMouseGridPosition();
        /*
        if (map.ContainsKey(gridPos))
        {
            string text;

            if (map[gridPos].contents.Count > 0)
            {
                text = map[gridPos].contents[0].Description;
                //TODO : to handle multiple entities
            }
            else
            {
                text = gridPos.ToString();
            }

            ShowToolTip(text);
            ShowGridPositionHighlight(gridPos);

            

        }
        else
        {
            HideToolTip();
            HideGridPositionHighlight();
        }
        */

        ///new game loop - work in progress
        ///

        /////
        /////Its been a while since I worked on this section. Why a queue for actions?
        /////Since only 1 action happens at a time, couldn't it be a single variable?
        
        if(isPlayerTurn)
        {
            if(actionQueue.Count == 0)
            {
                playControlOverlay.gameObject.SetActive(true);
                //we also need to activate keyboard input
            }
            else
            {
                playControlOverlay.gameObject.SetActive(false);
                //we also need to disable keyboard input
                //(other than viewport manipulation and menu options etc)

                //continue processing current action 
                //ie animation etc until we get to destination / complete action
                //if complete, remove action from queue
                // example:
                // bool MoveTo (Vector2Int dest) tweens to dest while aniumating until arrives.
                //returns false until complete
                IGameAction gameAction = actionQueue.Peek();
                if(gameAction.DoIt()) actionQueue.Dequeue();//remove from queue when completed
            }

        }
        else
        {

            //do AI processing
            playControlOverlay.gameObject.SetActive(false);

            if (actionQueue.Count == 0)
            {
                //process AI descisions, add to gameActions to queue
                if(opponent.Go())
                {
                    player.StartTurn();
                }
            }
            else
            {
                IGameAction gameAction = actionQueue.Peek();
                if (gameAction.DoIt()) actionQueue.Dequeue();//remove from queue when completed
            }

        }
        
    }

    private void HideGridPositionHighlight()
    {
        if (displayOverlay != null && highlight != null)
        {
            highlight.gameObject.SetActive(false);

        }
    }

    private void ShowGridPositionHighlight(Vector2Int position)
    {
        if (displayOverlay != null && highlight != null)
        {
            highlight.gameObject.SetActive(true);
            highlight.transform.position = TiledLevel.GetGridCentrePoint(position);

        }
    }

    public static Vector2Int GetMouseGridPosition()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //for if and when I switch to 3d
        //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        Vector2Int gridPos = TiledLevel.WorldToGrid(mousePos);
        return gridPos;
    }

    public void ShowToolTip(string text)
    {
        if (displayOverlay != null && toolTipText != null)
        {
            toolTipText.gameObject.SetActive(true);
            toolTipText.gameObject.GetComponentInChildren<Text>().text = text;
            RectTransform rect = toolTipText.gameObject.GetComponent<RectTransform>();
            Vector3 pos = new Vector3(Input.mousePosition.x + rect.sizeDelta.x / 2, Input.mousePosition.y, Input.mousePosition.z);
            rect.position = pos;


        }
    }

    public void HideToolTip()
    {
        if (displayOverlay != null && toolTipText != null)
        {
            toolTipText.gameObject.SetActive(false);

        }
    }

    
    public void MoveEntityToGridPosition(Entity entity, Vector2Int dest)
    {
        map.MoveEntity(entity, dest);
        
    }


}
