﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[System.ObsoleteAttribute("Use TiledLevel.Tile", true)]
public class Tile 
{
    public List<Entity> contents = new List<Entity>();
}
