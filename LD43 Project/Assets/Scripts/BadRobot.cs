﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadRobot : Entity
{
    public List<Vector2Int> waypoints = new List<Vector2Int>(); //patrol path
    int nextWaypoint = 0;
	// Use this for initialization
	void Start ()
    {
        Description = "Bad Robot";
        
    }

    override public int Type { get { return 1; } }

    override public bool Act(Game game)
    {
        Debug.Log(this.Description);
        if (gridPosition == waypoints[nextWaypoint])
        {
            nextWaypoint++;
            if(nextWaypoint==waypoints.Count)
            {
                nextWaypoint = 0;//loop
            }
        }
        else
        {
            destGridPosition = GetClosestNeighbourToTarget(waypoints[nextWaypoint]);
            //should be animated over several frames ....
            
            game.MoveEntityToGridPosition(this, destGridPosition);
            //Debug.Log(gridPosition.ToString());
        }
        return true;
    }

    override public void Reset()
    {
        
    }

    Vector2Int GetClosestNeighbourToTarget(Vector2Int target)
    {
        Vector2Int result = Vector2Int.zero; //current pos
        float shortestDistance = Vector2Int.Distance((gridPosition + result), target);
        Vector2Int test = Vector2Int.left;
        float distance = Vector2Int.Distance((gridPosition + test), target);
        if(distance < shortestDistance)
        {
            shortestDistance = distance;
            result = test;
        }

        test = Vector2Int.right;
        distance = Vector2Int.Distance((gridPosition + test), target);
        if (distance < shortestDistance)
        {
            shortestDistance = distance;
            result = test;
        }

        test = Vector2Int.up;
        distance = Vector2Int.Distance((gridPosition + test), target);
        if (distance < shortestDistance)
        {
            shortestDistance = distance;
            result = test;
        }

        test = Vector2Int.down;
        distance = Vector2Int.Distance((gridPosition + test), target);
        if (distance < shortestDistance)
        {
            shortestDistance = distance;
            result = test;
        }





        return gridPosition + result;
    }
}
