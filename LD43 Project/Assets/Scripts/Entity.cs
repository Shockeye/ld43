﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
abstract public class Entity : MonoBehaviour
{
    [SerializeField]
    protected Vector2Int gridPosition;
    protected Vector2Int destGridPosition; //entity will move here at end of turn

    private string description;

    public int prefabID;
    public int instanceID;

    public Vector2Int coordinates
    {
        get
        {
            return gridPosition;
        }

        set
        {
            gridPosition = value;
            destGridPosition = value;
        }
    }

    public Vector3 offset = Vector3.zero;//for adjusting position if object base is not at 0,0,0

    public string Description
    {
        get
        {
            return description;
        }

        protected set
        {
            description = value;
        }
    }

    abstract public int Type { get; }

    virtual public void EndTurn()
    {

    }

    abstract public bool Act(Game game); //do the enities action for the turn ; return true when turn completed
    abstract public void Reset();
}



/// <summary>
/// The following are the basic IGameAction implemtations that work for all Entities
/// </summary>
public class MoveTo : IGameAction
{
    Entity entity;
    Vector2Int destination;
    Vector3 position;
    float frame = 0;//bad name , need to work speed etc 
    float rate = 0.1f;//make public so configurable in inspector???
    
    public MoveTo(Entity entity, Vector2Int destination)
    {
        this.entity = entity;
        this.destination = destination;
        this.position = TiledLevel.GetGridCentrePoint(destination);
    }

    public bool DoIt()
    {
        //TODO: tween to destination

        entity.transform.position = Vector3.MoveTowards(entity.transform.position, position, frame);
        if (entity.transform.position != position)
        {
            frame += rate;
            return false;
        }
        else
        {
            //TODO:also update gridPosition of unit, and add to that Tile's contents.
            frame = 0;
            return true;
        }
    }
}