﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TileEditorWindow : EditorWindow
{
    public TiledLevel level;
   
    //todo: show selected tile coords.
    //todo: allow add entity
    //todo: add decor
    //todo: tile special types, spawn points waypoints, etc
    void OnGUI()
    {
        if (level == null) return;
        /*      GUILayout.Label("Base Settings", EditorStyles.boldLabel);
              myString = EditorGUILayout.TextField("Text Field", myString);

              groupEnabled = EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
              myBool = EditorGUILayout.Toggle("Toggle", myBool);
              myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
              EditorGUILayout.EndToggleGroup();
              */
        string msg = "Tile Coordinates: ";
        msg += level.Selected.ToString();
        GUILayout.Label(msg, EditorStyles.boldLabel);

        GUILayout.Label("Contents", EditorStyles.boldLabel);
        TiledLevel.Tile tile = level.GetTileAt(level.Selected.coordinates);
        foreach(int id in tile.contents)
        {
            Entity entity = level.GetEntity(id);
            //TODO: show entity in window, as button or check box ?? to delete or edit
            if (entity != null)
            {
                GUILayout.Button(entity.name);
            }
        }
        
        GUILayout.Label("Add Entity:", EditorStyles.boldLabel);

        foreach (var prefab in level.prefabs)
        {
            if (GUILayout.Button(prefab.name))
            {
                //TODO: instantiate and instance of this prefab 
                Debug.Log("entity added");
            }


        }
        GUILayout.Label("Drag and Drop New Entity Prefab to tile:", EditorStyles.boldLabel);

        DragAndDrop.visualMode = DragAndDropVisualMode.Generic;
        if (Event.current.type == EventType.DragExited)
        {
            foreach (Object obj in DragAndDrop.objectReferences)
            {
                Debug.Log(obj.ToString());
                if (obj is GameObject)
                {
                    /*
                     * TODO:
                     * As we will be direct dropping, we need to make decisions based 
                     * on object type here to sort it out.
                     */
                    
                    GameObject gameObject = obj as GameObject;
                    Entity ent = gameObject.GetComponent<Entity>();
                    if (ent != null)
                    {
                        DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                        //level.prefabs.Add(ent.gameObject.transform);
                        int prefabIndex = level.AddPrefab(ent.gameObject.transform);
                        //level.Selected.AddEntity(ent);
                        
                        //Transform objTransform = GameObject.Instantiate(ent.transform, level.gameObject.transform);
                        Transform objTransform = (Transform) PrefabUtility.InstantiatePrefab(ent.transform);//,, level.gameObject.transform);
                        if (objTransform != null)
                        {
                            objTransform.parent = level.gameObject.transform;
                            Entity e = objTransform.gameObject.GetComponent<Entity>();
                            if (e != null)
                            {
                                level.AddEntity( e,prefabIndex,level.Selected.coordinates);
                            }


                            /*
                             var myProperty =  serializedObject.FindProperty("propertyname");
                             EditorGUILayout.PropertyField(myProperty);
                             serializedObject.ApplyModifiedProperty();
                             */
                        }

                        
                    }
                    

                }
                
            }

        }
    }

}
