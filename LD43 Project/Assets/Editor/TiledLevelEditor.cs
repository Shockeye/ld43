﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TiledLevel))]
public class TiledLevelEditor : Editor
{
    bool redrawMesh = true;

    SerializedProperty prefabs;
    override public void OnInspectorGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();

        DrawDefaultInspector();
        
        TiledLevel level = (TiledLevel)target;
        EditorGUILayout.LabelField("Selected Tile:" );
        if (level.Selected != null)
        {
            
            EditorGUI.indentLevel++;
            EditorGUILayout.LabelField("Coordinates", level.Selected.coordinates.ToString());
            EditorGUI.indentLevel--;
        }
        else
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.LabelField("none selected");
            EditorGUI.indentLevel--;
        }

        if(GUILayout.Button("save"))
        {
            //export current level to file
            level.Export(level.fileName);
        }
        if (GUILayout.Button("load"))
        {
            //clear current level ...TODO: warning, dirty flag etc
            level.ClearLevel();
            Transform[] children = level.GetAllChildren();
            for (int i = children.Length - 1; i >= 0; i--)
            {
                DestroyImmediate(children[i].gameObject);
            }

            //load level
            level.Load(level.fileName);
            Debug.Log("level loaded...");

            level.InitMesh();
            Debug.Log("mesh init...");

            level.BuildMesh();
            Debug.Log("mesh built.");

        }

        if (GUILayout.Button("clear"))
        {
            //TODO: warning
            level.ClearLevel();
            Transform[] children = level.GetAllChildren();
            for (int i = children.Length - 1; i >= 0; i--)
            {
                DestroyImmediate(children[i].gameObject);
            }

        }

        serializedObject.ApplyModifiedProperties();
    }

    

    private void OnSceneGUI()
    {
        serializedObject.UpdateIfRequiredOrScript();

        GUIStyle style = new GUIStyle();
        style.alignment = TextAnchor.MiddleCenter;
        Quaternion direction = Quaternion.LookRotation(Vector3.up);
  

        TiledLevel level = (TiledLevel)target;
        level.transform.position = Vector3.zero;

        Vector2Int[] emptyGridPositions = level.GetEmptyGridPositions();
        foreach (var item in emptyGridPositions)
        {
            //Handles.Label(TiledLevel.GridToWorld(item), "Add Tile");//, style);
            bool result = Handles.Button(TiledLevel.GridToWorld(item), direction, 0.1f, 0.1f, Handles.RectangleHandleCap);
            if(result)
            {
                level.AddTile(item);
                redrawMesh = true;
                level.SelectTile(item);
                ShowTileEditorWindow(level);
                             
            }
            
        }

        Vector2Int[] tileGridPositions = level.GetTileGridPositions();
        foreach (var item in tileGridPositions)
        {
            Handles.color = Color.yellow;
            bool result = Handles.Button(TiledLevel.GridToWorld(item), direction, 0.5f, 0.5f, Handles.RectangleHandleCap);
            if (result)
            {
                level.SelectTile(item);
                ShowTileEditorWindow(level);

            }

        }

        if (redrawMesh)
        {
            
            level.BuildMesh();
            redrawMesh = false;
        }

        serializedObject.ApplyModifiedProperties();
    }

    private static void ShowTileEditorWindow(TiledLevel level)
    {
        // Get existing open window or if none, make a new one:
        TileEditorWindow window = (TileEditorWindow)EditorWindow.GetWindow(typeof(TileEditorWindow));
        window.titleContent = new GUIContent("Edit Tile");
        window.level = level;
        window.Show();
    }

    private void OnEnable()
    {
        Tools.hidden = true;
        
        prefabs = serializedObject.FindProperty("prefabs");
    }

    private void OnDisable()
    {
        Tools.hidden = false;
    }
    
}
